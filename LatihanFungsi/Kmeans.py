import numpy as np

from matplotlib import pyplot as plt
from sklearn.cluster import KMeans

def prepare_plt(list_array):
    x_ = []
    y_ = []
    for i in X:
        x_.append(i[0])
        y_.append(i[1])
    return x_,y_

X = np.array([[1, 2], [1, 4], [1, 0], [4, 2], [4, 4], [4, 0]])

kmeans = KMeans(n_clusters=2,random_state=0).fit(X)
label = kmeans.labels_
result = kmeans.predict([[0,0],[4,4]])
centroid = kmeans.cluster_centers_

print('centroid:',centroid)
print(label)
print(result)

x_,y_ = prepare_plt(X)

plt.scatter(x_,y_, c='black',s=7,alpha = 0.5)
plt.scatter(centroid[0], centroid[1], marker='*', s=200, c='g')
plt.show()
