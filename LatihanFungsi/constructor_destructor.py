from gensim.models.doc2vec import Doc2Vec, TaggedDocument

class FooType():
    def __init__(self, id):
        self.id = id
        self.model = None
        print (self.id, 'born')
    
    def coba(self):
        self.model = Doc2Vec.load('d2v.model')

    def __del__(self):
        print(self.id, 'died')
        print(self.model, 'died')


ft = FooType(1)
ft.coba()
print(ft.model)
del ft
