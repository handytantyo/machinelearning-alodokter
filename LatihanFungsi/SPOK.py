import re

def clean_str(string):
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r",", " ", string)
    string = re.sub(r"! !", "", string)
    string = re.sub(r"  ", " ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " ( ", string)
    string = re.sub(r"\)", " ) ", string)
    string = re.sub(r"\?", " ? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip().lower()

def n_grams(sentence):
    words = sentence.split()
    #print(words)
    result = []
    new_sentence = ''
    batas_spok = 4
    
    for batas in range(batas_spok):
        for start in range(len(words)-batas):
            for i in range(start,start+(batas+1)):
                new_sentence += words[i]
                if i != start + batas:
                    new_sentence += ' '
            result.append(new_sentence)
            new_sentence = ''
    return result

string = 'Today is Monday and tomorrow is tuesday'
string = clean_str(string)
result = n_grams(string)
print(result)