import random as rn
import numpy as np
import datetime

def permutation(maks_angka):
    result = []
    stop_checker = 0
    while stop_checker != angka:
        temp = rn.randint(0,angka-1)
        if temp not in result:
            result.append(temp)
            stop_checker += 1
    
    return result

def fibonaci(array):
    if array <= 1:
        return 1
    else:
        return fibonaci(array-1) + fibonaci(array-2)

def binary(angka):
    if angka == 0:
        return 0
    else:
        return (angka%2) + 10*binary(angka//2)

def ganti_jam(string_date,jam):
    sesudah = string_date.index(':')
    jam_digit_belakang = int(string_date[sesudah-1])
    jam_digit_belakang += jam
    jam_digit_depan = int(string_date[sesudah-2])
    if jam_digit_belakang >= 10:
        jam_digit_depan += jam_digit_belakang//10
        jam_digit_belakang = jam_digit_belakang % 10
    elif jam_digit_belakang < 0:
        jam_digit_depan -= jam_digit_belakang//10
        jam_digit_belakang = jam_digit_belakang % 10
    
    stop = jam_digit_depan*10 + jam_digit_belakang
    if stop >= 24:
        return 'None'

    #slicing
    string_date = string_date[:(sesudah-2)] + str(jam_digit_depan) + str(jam_digit_belakang) + string_date[sesudah:]
    
    return string_date


angka = 7
hasil = permutation(angka)
print(hasil)
print(fibonaci(6))
print(binary(8))

array_ = [data for data in range(7) if data%2 == 1]
print(array_)

array2_ = [[0]*2 for data in range(3)]
print(array2_)

array3_ = [data for data in range(7) for i in range(data)]
print(array3_)

#array [0] aja yang diambil
#rtime = 0.0
#rtime = input().split()[0]
#print(rtime)

#tipe data
rtime = [(2,3,4),(7,5)]
print(type(rtime))
array4_ = [
    data for data in range(7)
    for i in range(2)
    if data % 2 == 1
]

print(array4_)

#shape
array5_ = [[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]]
array6_ = [0,0,0],[0,0,0]
shape = np.shape(array5_)
shape1 = np.shape(array6_)
print(shape, shape1)

date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
print(date)
print(ganti_jam(date,7))

array7_ = [[1,3,4,5]]
print(np.shape(array7_))

s = [2, 3, 1, 4, 5]
print(sorted(range(len(s)), key=lambda k: s[k]))
