def check_digit_in_order(number):
    # split number into list
    list_per_number = []
    while number != 0:
        list_per_number.append(number%10)
        number = number//10
    
    # sorting list ascending
    checker = sorted(list(list_per_number),reverse=True)

    # return result
    if list_per_number == checker:
        return True
    else:
        return False

def app(environ, start_response):
    """Simplest possible application object"""
    data = b'Hello, World!\n'
    status = '200 OK'
    response_headers = [
        ('Content-type', 'text/plain'),
        ('Content-Length', str(len(data)))
    ]
    start_response(status, response_headers)
    return iter([data])

a = 14789
b = check_digit_in_order(a)
print(b)

list_number = [1,2,3,4]
print(*list_number)

# inside a function header:
# * collects all the positional arguments in a tuple.
# ** collects all the keyword arguments in a dictionary.

# In a function call:
# * unpacks a list or tuple into position arguments.
# ** unpacks a dictionary into keyword arguments.