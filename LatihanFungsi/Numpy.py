import numpy as np
import keras

from keras.layers import Input, Embedding, LSTM, Dense
from keras.models import Model

x_train = np.random.random((100, 100, 100, 3))
# size, 10 untuk batas low jumlah disamakan dengan num_classes, 100 untuk panjang baris, 1 untuk panjang kolom
y_train = keras.utils.to_categorical(np.random.randint(10, size=(100, 1)), num_classes=10) 

print(x_train.shape)
print(y_train.shape)
#print(y_train)

#embedding
# Headline input: meant to receive sequences of 100 integers, between 1 and 10000.
# Note that we can name any layer by passing it a "name" argument.
main_input = Input(shape=(100,), dtype='int32', name='main_input')

# This embedding layer will encode the input sequence
# into a sequence of dense 512-dimensional vectors.
x = Embedding(output_dim=512, input_dim=10000, input_length=100)(main_input)

# A LSTM will transform the vector sequence into a single vector,
# containing information about the entire sequence
lstm_out = LSTM(32)(x)
print(lstm_out)