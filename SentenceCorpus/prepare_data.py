from keras.preprocessing.text import Tokenizer
from sklearn.preprocessing import LabelEncoder
from keras import models
from keras import layers
from numpy import zeros
from random import shuffle
from sklearn.externals import joblib

import re
import glob
import errno
import keras
import numpy as np
import loadModel
import json
import keras.preprocessing.text as kpt

#1. read all of sentences in all files
def read_lines(path):
    #path = 'SentenceCorpus/labeled_articles/*.txt'
    #read all of file in the selected path
    files = glob.glob(path)
    train_lines = []

    for name in files:
        try:
            with open(name) as f:
                for line in f.readlines():
                    if line.startswith('#', 0):
                        continue
                    train_lines.append(line)
        except IOError as exc:
            if exc.errno != errno.EISDIR:
                raise
    
    print('Total kalimat yang diambil adalah',len(train_lines))#total 3297 kalimat
    #check train_line sudah tidak ada ###abstract###
    #print(train_line[0].startswith('#',0))
    shuffle(train_lines)

    return train_lines

#2. split line to data(x_train) and label(y_train)
def split_lines(list_kalimat):
    data = []
    label = []
    for line in list_kalimat:
        if "AIMX" in line:
            label.append('AIM')
            data.append(line.replace('AIMX','').replace('\t',''))
        elif "OWNX" in line:
            label.append('OWN')
            data.append(line.replace('OWNX','').replace('\t',''))
        elif "CONT" in line:
            label.append('CONTRAST')
            data.append(line.replace('CONT','').replace('\t',''))
        elif "BASE" in line:
            label.append('BASIS')
            data.append(line.replace('BASE','').replace('\t',''))
        elif "MISC" in line:
            label.append('MISC')
            data.append(line.replace('MISC','').replace('\t',''))
    
    return data,label

def clean_str(data):
    lower_words = []
    for string in data:        
        string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
        string = re.sub(r",", " ", string)
        string = re.sub(r"!", " ! ", string)
        string = re.sub(r"\(", " ( ", string)
        string = re.sub(r"\)", " ) ", string)
        string = re.sub(r"\?", " ? ", string)
        string = re.sub(r"\s{2,}", " ", string)
        lower_words.append(string.lower())
    return lower_words

def convert_text_to_index_array(text, dictionary):
    return [dictionary[word] for word in kpt.text_to_word_sequence(text)]

#3. prepare data into index array(number)
def prepare_data(data,label):
    #one-hot encoding for label
    label_encoder = LabelEncoder()
        #change value label into its class, value will be integer
    label = label_encoder.fit_transform(label)

    #save untuk digunakan di predict
    np.save('review-classes.npy', label_encoder.classes_)

    #change label value into categorical matrix([0. 0. 1. 0. 0.])
    label = keras.utils.to_categorical(label,5)

    #tokenizer for dictionary
    tokenizer = Tokenizer(num_words=2000)
    tokenizer.fit_on_texts(data)
    
    #make dictionary to be dict set {'the':1 , 'of':2, ...}
    dictionary = tokenizer.word_index
    #print('Panjang Dictionary',len(dictionary))
    
    # Let's save this out so we can use it for prediction
    with open('review-dictionary.json', 'w') as dictionary_file:
        json.dump(dictionary, dictionary_file)

    #__lookup_text_to_index_array
    allWordIndices = []
    for text in data:
        wordIndices = convert_text_to_index_array(text,dictionary)
        allWordIndices.append(wordIndices)

    
    #one-hot metrics for data
        #allwordindices change value(integer) of selected data to be position of each word until length of selected sentence [1,1219,3,2310,..]
    allWordIndices = np.asarray(allWordIndices)
        #make foreach data into array that length equal with dictionary and value according to position [0. 1. 1. ... 0. 0. 1.]
    data = tokenizer.sequences_to_matrix(allWordIndices,mode='binary')

    # Let's save this out so we can use it later, file format pickle
    joblib.dump(tokenizer, 'review-tokenizer.pkl')

    return data,label
