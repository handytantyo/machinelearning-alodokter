import prepare_data
import loadModel

#main
#1 write file into array
list_kalimat = prepare_data.read_lines('SentenceCorpus/labeled_articles/*.txt')
#print(list_kalimat[0])

#2 split list into data and label
data,label = prepare_data.split_lines(list_kalimat)
data = prepare_data.clean_str(data)

#3 prepare data
data,label = prepare_data.prepare_data(data,label)
#print(data[0])
#print(label[0])

#4 train model, 2000 data diambil berdasarkan frekuensi kata tersebut ada di dalam training data
model = loadModel.get_model(2000)
loadModel.train_model(model,data,label,True) #true for continuing last model

#5 predict -> di predict.py