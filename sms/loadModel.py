from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import Conv1D, GlobalMaxPooling1D
from keras.preprocessing.text import Tokenizer
from keras.preprocessing import sequence
from sklearn.preprocessing import LabelEncoder
import time
from keras import metrics
from keras.callbacks import ModelCheckpoint
import keras.preprocessing.text as kpt

#training function
def get_model(num_max):
    model = Sequential()
    model.add(Dense(512, activation='relu', input_shape=(num_max,)))
    model.add(Dropout(0.2))
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(3, activation='sigmoid')) #kalo 1 harus menggunakan one hot encoder
    model.summary()
    model.compile(loss='categorical_crossentropy',#binary_crossentropy
              optimizer='adam',
              metrics=['accuracy'])
    #save model
    checkpointer = ModelCheckpoint('review-model-{epoch:02d}.hdf5', verbose=1)
    print('compile done')
    return model

def train_model(model,x,y):
    #make a checkpoint for saving file
    checkpoint = ModelCheckpoint('model-checkpoint/review-model-{epoch:02d}.hdf5', verbose=1)
    #checkpoint = ModelCheckpoint('weights.{epoch:03d}-{val_acc:.4f}.hdf5', monitor='val_acc', verbose=1, save_best_only=True, mode='auto')
    model.fit(x,y,
                batch_size=32,
                epochs=10,
                verbose=1,
                callbacks = [checkpoint],
                shuffle = True,
                validation_split=0.2)
            # save the model architecture & model weight
    model_json = model.to_json()
    with open('model-checkpoint/review-model.json', 'w') as json_file:
        json_file.write(model_json)
