import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import keras
import os
import re
import time
import datetime
import numpy as np
import string
import nltk
import json
import keras
import keras.preprocessing.text as kpt
import random as rn
#import simple_model as CobaModel

from keras.preprocessing.text import Tokenizer
from sklearn.preprocessing import LabelEncoder
from numpy import array
from pandas import DataFrame
from sklearn.externals import joblib
from sklearn.preprocessing import LabelEncoder
from keras.models import Sequential
from keras.models import model_from_json
from keras.layers import Dense, Dropout, Activation
from keras.callbacks import ModelCheckpoint

def PermutateIndex(array,number):
    panjang = 0
    while panjang < number:
        a = rn.randint(0, number-1)
        if a not in array:
            array.append(a)
            panjang += 1

    return array

def clean_str(string):
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r",", " ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " ( ", string)
    string = re.sub(r"\)", " ) ", string)
    string = re.sub(r"\?", " ? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip().lower()


def x_train_y_train(teks,label,num_max):

    #preprocessing

    #one-hot encoding digunakan untuk kelas yang lebih dari 2
    le = LabelEncoder()
    label = le.fit_transform(label)

    #save untuk digunakan di predict
    np.save('review-classes.npy', le.classes_)

    label = keras.utils.to_categorical(label,3)#dibuat menjadi 3 kelas

    #untuk x harus di tokenizer agar data latihnya dalam bentuk matrix
    tok = Tokenizer(num_words=num_max)    
    index = 0
    for i in teks:
        teks[index] = clean_str(i)
        index+=1
        
    tok.fit_on_texts(teks)

    return teks, label, tok

def convert_text_to_index_array(text, dictionary):
    return [dictionary[word] for word in kpt.text_to_word_sequence(text)]