from flask import Flask, request, jsonify,render_template
#from ml import model
app = Flask(__name__)
#model.train()
post1 = [
    {
        'author' : 'Orang 1',
        'title' : 'coba1',
        'content' : 'Today is Wednesday',
        'date_posted' : '1 January 2018'
    },
    {
        'author' : 'Orang 2',
        'title' : 'coba2',
        'content' : 'Tomorrow is Thursday',
        'date_posted' : '2 January 2018'
    }
]



@app.route("/test/<int:a>/<int:b>",methods=['GET'])
def test(a,b):
    response = a + b
    return str(response) 

@app.route('/post',methods=['POST'])
def post():
    b = request.json
    #.form, .json
    print (b)
    #result =model.predict(request.json) 
    return jsonify(b)

@app.route('/processjson',methods=['POST'])
def processjson():
    data = request.get_json()
    name = data['name']
    location = data['location']

    return jsonify({'result' : 'Success', 'name':name, 'location' : location})

@app.route('/basic',methods=['GET','POST'])
def basic():
    #request.method and request.form
    print(request.method)
    if request.method == 'POST':
        data = request.form['name']
        return data
    return render_template('basic.html',posts = post1)


#to run this program
if __name__ == '__main__':
    app.run(debug=True)