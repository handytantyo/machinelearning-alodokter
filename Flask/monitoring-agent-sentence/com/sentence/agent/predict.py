import re
import time
import datetime
import numpy as np
import string
import json
import keras
import keras.preprocessing.text as kpt
import os

from numpy import array
from pandas import DataFrame
from sklearn.externals import joblib
from sklearn.preprocessing import LabelEncoder
from keras.preprocessing.text import Tokenizer
from keras.models import Sequential
from keras.models import model_from_json
from keras.layers import Dense, Dropout, Activation
from keras.callbacks import ModelCheckpoint


class Predict:
    #predict function
    def load(self):
        # read in your saved model structure
        #print(os.getcwd())
        model_structure_file = open('./model-checkpoint/review-model.json', 'r')
        loaded_model_json = model_structure_file.read()
        model_structure_file.close()

        # and create a model from that
        model = model_from_json(loaded_model_json)
        model.load_weights('./model-checkpoint/review-model-10.hdf5')

        # read in our saved classes
        le = LabelEncoder()
        le.classes_ = np.load('./review-classes.npy')

        # read in our saved dictionary
        with open('./review-dictionary.json', 'r') as dictionary_file:
            dictionary = json.load(dictionary_file)

        # read in our saved tokenizer
        tokenizer = joblib.load('./review-tokenizer.pkl')

        return model, le, tokenizer, dictionary
    
    def predict(self,text):
        model, le, tok, dictionary = self.load()
        labels = le.classes_

        #referensi lookup_text_to_index_array
        words = kpt.text_to_word_sequence(text)
        
        test = []
        for word in words:
            if word in dictionary:
                test.append(dictionary[word])
            else:
                pass

        test = tok.sequences_to_matrix([test], mode='binary')

        # predict which bucket your input belongs in
        pred = model.predict(test)
        
        #clear the session for preventing later error in post/get
        keras.backend.clear_session()

        # and print it for the humons
        #print("%s class; %f%% confidence" % (labels[np.argmax(pred)], pred[0][np.argmax(pred)] * 100))
        #print(pred)

        return labels[np.argmax(pred)],pred[0][np.argmax(pred)] * 100

    def predict1(self,text):
        model, le, tok, dictionary = self.load()
        labels = le.classes_

        #referensi lookup_text_to_index_array
        words = kpt.text_to_word_sequence(text)
        
        test = []
        for word in words:
            if word in dictionary:
                test.append(dictionary[word])
            else:
                pass

        test = tok.sequences_to_matrix([test], mode='binary')

        # predict which bucket your input belongs in
        pred = model.predict(test)
        
        #clear the session for preventing later error in post/get, Destroys the current TF graph and creates a new one.
        #Useful to avoid clutter from old models / layers.
        keras.backend.clear_session()

        # and print it for the humons
        print("%s class; %f%% confidence" % (labels[np.argmax(pred)], pred[0][np.argmax(pred)] * 100))
        #print(pred)

        return labels.tolist(),(pred[0]*100).tolist(),np.argsort(pred[0]).tolist()#labels[np.argmax(pred)],pred[0][np.argmax(pred)] * 100

#predict('We propose quantum effect  symbol , which leads to a search strategy fit to clustering')
#predict('Today is Wednesday')