import traceback

from flask import current_app
from flask import Flask, request, jsonify, make_response
from flask_restful import Resource, Api
from com.sentence.agent.predict import Predict


app = Flask(__name__)
#app.config.from_pyfile('settings/settings.cfg')
api = Api(app)

predict_agent = Predict()

@app.route("/post",methods=['POST'])
def get():
    try:
        text = request.get_json()
        predict,confidence = predict_agent.predict(text)
        app.logger.info('[Monitor] '+ str(predict))

        data = {}
        data['predict'] = predict
        data['confidence'] = str(round(confidence,2))+'%'

        return make_response(jsonify(data=data), 200)#jsonify(data)

    except:
        app.logger.info(traceback.format_exc())
        return make_response(jsonify(data='None'), 200)
    
#api.add_resource(PredictAgent, '/monitoring-agent-sentence/predict')

if __name__ == '__main__':
    app.run(debug=True)
