import traceback

from flask import current_app
from flask import Flask, request, jsonify, make_response
from flask_restful import Resource, Api
from com.sentence.agent.predict import Predict
import json
import re
import numpy as np

app = Flask(__name__)
#app.config.from_pyfile('settings/settings.cfg')
api = Api(app)

predict_agent = Predict()

class PredictAgent(Resource):
    #name must match with the method
    def post(self):
        try:
            text = request.get_json()
            predict, confidence = predict_agent.predict(text)
            app.logger.info('[Predicted] as '+ str(predict))
            
            data = {}
            data['text'] = text
            data['predict'] = predict
            data['confidence'] = confidence#str(round(confidence,2))+'%'

            return make_response(jsonify(data=data), 200)

        except:
            app.logger.info(traceback.format_exc())
            return make_response(jsonify(data='None'), 200)
    
    def get(self):
        try:
            text = ['Today is Monday',
                    'So far the approach has been developed for discrete variable models on a more abstract  CITATION  versus practical level  CITATION',
                    'In this paper we apply the idea to graphical models for continuous variables',
                    'In this paper we derive the equations for Loop Corrected Belief Propagation on a continuous variable Gaussian model',
                    'Ini bahasa Indonesia',
                    'The principal requirement  is that the magnitude of pair variable cumulants of cavity distributions is an order smaller than the  single variable cumulants, and third order cumulants are even smaller, etc',
                    'Using the exactness of the averages for belief propagation for Gaussian models, a  different way of obtaining the covariances is found,  based on Belief Propagation on cavity graphs',
                    'A by-product of our loop corrected belief propagation equations is an algorithm that calculates exact covariance matrices for Gaussian models like the one discussed in  CITATION , but without explicitly using linear response',
                    'A more flexible regressor can fit more data  SYMBOL  well than a more rigid one',
                    'Section  contains further considerations, to be elaborated on in the future',
                    ]

            #type must list, dict has no attribute append
            data_struct = []
            for i in text:
                predict_,confidence_ = predict_agent.predict(i)
                confidence_ = str(round(confidence_,2))+'%'
                data_struct.append({'text':i, 'predict':predict_, 'confidence':confidence_})

            #app.logger.info('[Predicted] as '+ str(predict))

            return make_response(jsonify(data=data_struct), 200)#json.dumps(data_struct)

        except:
            app.logger.info(traceback.format_exc())
            return make_response(jsonify(data='None'), 200)


class PredictAgentGet(Resource):
    #name must match with the method
    def get(self,text):
        try:
            predict, confidence = predict_agent.predict(text)
            app.logger.info('[Predicted] as '+ str(predict))
            
            data = {}
            data['predict'] = predict
            data['confidence'] = str(round(confidence,2))+'%'

            return make_response(jsonify(data=data), 200)

        except:
            app.logger.info(traceback.format_exc())
            return make_response(jsonify(data='None'), 200)

class PredictAgentPostModif(Resource):
    #name must match with the method
    def post(self):
        predict = []
        confidence = []
        index_sorted = []
        try:
            text = request.get_json()

            #call all prediction, confidence and index confidence ascending
            predict, confidence, index_sorted = predict_agent.predict1(text)
            app.logger.info('[Predicted] as '+ str(predict))
            
            #make descending
            sorted_predict = []
            index_sorted = index_sorted[::-1]
            confidence = sorted(confidence,reverse=True)
            
            #combine sorted_predict with its confidence
            rows_to_insert = []
            index = 0            
            for i in index_sorted:
                sorted_predict.append(predict[i])
                #predict_print.append(str(sorted_predict[index]) + ' : ' + str(round(confidence[index],2))+'%')
                data_struct = (sorted_predict[index],confidence[index])
                rows_to_insert.append(data_struct)
                index+=1

            #combine text and predict_print into data_struct(tuple)
            return make_response(jsonify(text=text,data=rows_to_insert), 200)
        
        except:
            app.logger.info(traceback.format_exc())
            return make_response(jsonify(data='None'), 200)
    
api.add_resource(PredictAgent, '/monitoring-agent-sentence/predict')
api.add_resource(PredictAgentPostModif, '/monitoring-agent-sentence/predict1')
api.add_resource(PredictAgentGet,'/monitoring-agent-sentence/predict/<string:text>')

if __name__ == '__main__':
    app.run(debug=True)