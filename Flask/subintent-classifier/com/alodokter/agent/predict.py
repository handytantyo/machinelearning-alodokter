import re
import time
import datetime
import numpy as np
import string
import json
import keras
import keras.preprocessing.text as kpt
import os

from numpy import array
from pandas import DataFrame
from sklearn.externals import joblib
from sklearn.preprocessing import LabelEncoder
from keras.preprocessing.text import Tokenizer
from keras.models import Sequential
from keras.models import model_from_json
from keras.layers import Dense, Dropout, Activation
from keras.callbacks import ModelCheckpoint


class Predict:
    #predict function
    def load(self):
        # read in your saved model structure
        model_structure_file = open('model-checkpoint/subintent-model.json', 'r')
        loaded_model_json = model_structure_file.read()
        model_structure_file.close()

        # and create a model from that
        self.model = model_from_json(loaded_model_json)
        self.model.load_weights('model-checkpoint/subintent-model-82.h5')
        
        # read in our saved classes
        self.label_encoder = LabelEncoder()
        self.label_encoder.classes_ = np.load('subintent-classes.npy')
        
        # read in our saved dictionary
        with open('subintent-dictionary.json', 'r') as dictionary_file:
            self.dictionary = json.load(dictionary_file)
        
        # read in our saved tokenizer
        self.tokenizer = joblib.load('subintent-tokenizer.pkl')

    
    def __lookup_text_to_index_array(self, text):
        words = kpt.text_to_word_sequence(text)
        wordIndices = []
        for word in words:
            if word in self.dictionary:
                wordIndices.append(self.dictionary[word])
            else:
                pass
        return wordIndices  


    def predict(self,text):
        self.load()
        labels = self.label_encoder.classes_
        
        # create one-hot matrices
        test = self.__lookup_text_to_index_array(text)
        test = self.tokenizer.sequences_to_matrix([test], mode='binary')
        
        # predict which bucket your input belongs in
        pred = self.model.predict(test)

        # and print it for the humons
        #print("subintent: %s; confidence: %f%%" % (labels[np.argmax(pred)], pred[0][np.argmax(pred)] * 100))
        indices = np.argsort(-pred[0])[:10]
        
        #print("\nOTHER SUBINTENTS: ")                
        #for i in range(1, 10):
        #    print('%s: %f%%' % (labels[indices[i]], pred[0][indices[i]] * 100))
        
        labels_print = []
        confidence_print = []
        for i in range(10):
            labels_print.append(labels[indices[i]])
            confidence_print.append(pred[0][indices[i]])


        keras.backend.clear_session()

        return labels_print, confidence_print
        
#if __name__ == '__main__':
#    obj = Predict()
#    obj.load()
#    q = 'selamat siang dok.....dok jari tangan saya yang tengah tiba tiba tidak bisa dibalikkan ke lurus lagi....terasa sakit..jika dikembalikan luris harus dibantu dan dipaksa..kenapa yach dokm.terima kasih dok'
#    print('QUESTION:')
#    print(q)
#    print('\nPREDICTION RESULT:')
#    obj.predict(q)