import traceback

from flask import current_app
from flask import Flask, request, jsonify, make_response
from flask_restful import Resource, Api
from com.alodokter.agent.predict import Predict
import json
import re
import numpy as np

app = Flask(__name__)
api = Api(app)

predict_agent = Predict()

class PredictAgentPost(Resource):
    #name must match with the method
    def post(self):
        predict = []
        confidence = []
        try:
            data = request.get_json()
                      
            #input variable
            text = data['data']['body']
            title = data['data']['title']           

            #call top 10 prediction and confidence
            predict, confidence = predict_agent.predict(text)
            app.logger.info('[Predicted] as '+ str(predict[0]))

            #put predict and confidence into tuple rows_to_insert
            rows_to_insert = []         
            for i in range(10):
                data_struct = (predict[i],str(confidence[i]))
                rows_to_insert.append(data_struct)
            
            data = {"body":text,"subintent":rows_to_insert,'title':title}
            return make_response(jsonify(data=data), 200)


        except:
            app.logger.info(traceback.format_exc())
            return make_response(jsonify(prediction='None'), 200)
    
api.add_resource(PredictAgentPost, '/predict-agent/predict')

if __name__ == '__main__':
    app.run(debug=True)