package main

import (
	"fmt"
	"encoding/json"
	"os"
	"io"
	"sort"
	"golang.org/x/crypto/bcrypt"
)

type person struct {
	// harus upper case
	// json package only stringfiy fields start with capital letter. see http://golang.org/pkg/encoding/json/
	First string
	Last string
	Age int
}

type secretAgent struct {
	person
	ltk bool
}

type byName []person

func (bn byName) Len() int { return len(bn) }
func (bn byName) Swap(i int, j int) {bn[i], bn[j] = bn[j], bn[i]}
func (bn byName) Less(i, j int) bool {return bn[i].First < bn[j].First}

func main() {
	// Marshall
	p1 := person {
		First : "James",
		Last : "Bond",
		Age : 40,
	}

	p2 := person {
		First : "Stephen",
		Last : "Curry",
		Age : 29,
	}

	p3 := person {
		First : "Handy",
		Last : "Tantyo",
		Age : 23,
	}

	people := []person{p1, p2, p3}
	fmt.Println(people)

	bs, err := json.Marshal(people)
	if err != nil {	
		fmt.Println(err)
	}
	fmt.Println(string(bs))

	// Unmarshall
	s := `[{"First":"James","Last":"Bond","Age":40},{"First":"Stephen","Last":"Curry","Age":29}]`
	bs_json := []byte(s)
	fmt.Printf("%T\n",bs_json)
	fmt.Println(string(bs_json))

	// var people_unmarshal []person
	people_unmarshal := []person{}
	err2 := json.Unmarshal(bs_json, &people_unmarshal)
	if err2 != nil {
		fmt.Println(err2)
	}
	fmt.Println("All data :",people_unmarshal)

	for key, value := range people_unmarshal {
		fmt.Println(key, value.First, value.Last, value.Age)
	}

	// writer interface, standard library dari go banyak pilihan untuk print
	fmt.Println("Hai")
	fmt.Fprintln(os.Stdout, "Haii")
	io.WriteString(os.Stdout, "Haiii\n")

	//sorted
	aa := []int{23, 566, 12, 54, 1, 454, 6}
	fmt.Println(aa)
	sort.Ints(aa) //standard library go
	fmt.Println(aa)

	bb := []string{"Handy", "Dika", "Filbert", "Darren", "Fajar", "Richard"}
	fmt.Println(bb)
	sort.Strings(bb)
	fmt.Println(bb)

	// costum search
	fmt.Println(people)
	sort.Sort(byName(people))
	fmt.Println(people)

	// bcrypt
	password_ := `handy 123`
	encrypt_password := password(password_)
	fmt.Println(encrypt_password)

	loginPassword := `handy 123`
	err3 := bcrypt.CompareHashAndPassword(encrypt_password, []byte(loginPassword))
	if err3 != nil {
		fmt.Println("You can't login")
		return
	}
	fmt.Println("You're logged in")

}

func password(s string) []byte{
	// install bcrypt
	// go get  golang.org/x/crypto/bcrypt --> install
	// got get -u  golang.org/x/crypto/bcrypt --> update
	
	bs, err := bcrypt.GenerateFromPassword([]byte(s), bcrypt.MinCost) 
	if err != nil {
		fmt.Println(err)
	}
	// fmt.Println(s)
	// fmt.Println(bs)
	// fmt.Printf("%T\n", bs)

	return bs

}