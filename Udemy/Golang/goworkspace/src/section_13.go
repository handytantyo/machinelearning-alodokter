package main

import (
	"fmt"
)

type person struct {
	first_name string
	last_name string
	fav_ice_cream []string
}

type vehicle struct {
	doors int
	color string
}

type truck struct {
	vehicle
	fourWheel bool
}

type sedan struct {
	vehicle
	luxury bool
}

func main() {
	// exercise_1_2()
	exercise_3()
	exercise_4()
}

func exercise_1_2() {
	a := person {
		first_name : "Handy",
		last_name : "Tantyo",
		fav_ice_cream : []string{
			"Vanilla",
			"Strawberry",
		},
	}

	b := person {
		first_name : "James",
		last_name : "Bond",
		fav_ice_cream : []string{
			"Chocolate",
			"Mocca",
		
		},
	}

	fmt.Println(a.first_name, a.last_name)
	for index, value := range a.fav_ice_cream {
		fmt.Printf("\t%d\t%s\n", index, value)
	}

	fmt.Println(b.first_name, b.last_name)
	for index, value := range b.fav_ice_cream {
		fmt.Printf("\t%d\t%s\n", index, value)
	}
	
	fmt.Println("----------")
	
	// change into map format
	z := map[string]person {
		a.last_name : a,
		b.last_name : b,
	}

	for _, value := range z {		
		fmt.Println(value.first_name, value.last_name)
		for index, v := range value.fav_ice_cream {
			fmt.Printf("\t%d\t%s\n", index, v)
		}
	}
	
}

func exercise_3() {
	truck1 := truck {
		vehicle : vehicle {
			doors : 2,
			color : "yellow",
		},
		fourWheel : true,
	}

	sedan1 := sedan {
		vehicle : vehicle {
			doors : 4,
			color : "silver",
		},
		luxury : true,
	}

	fmt.Println(truck1)
	fmt.Println(sedan1)
	fmt.Println(truck1.doors, truck1.color)
	fmt.Println(sedan1.doors, sedan1.color)
}

func exercise_4() {
	p1 := struct {
		first_name string
		last_name string
		skill []string
		favorite map[string]string
		age int
	}{
		first_name : "Handy",
		last_name : "Tantyo",
		skill : []string{
			"Golang",
			"Python",
			"C#",
		},
		age : 23,
		favorite : map[string]string{
			"drink" : "water",
			"food" : "nasi goreng",
		},
	}

	fmt.Println(p1.first_name, p1.last_name)
	for index, value := range p1.skill {
		fmt.Printf("\t%d %s\n", index, value)
	}

	for key, value := range p1.favorite {
		fmt.Printf("\t%s %s\n", key, value)
	}

}
