package main

import (
	"fmt"
	"runtime"
	"sync"
)

func init() {
	// merupakan program yang dijalankan paling pertama dan hanya di jalankan 1 kali
	// biasa untuk set variable2 yang dibutuhkan
}

var wg sync.WaitGroup // untuk trigger concurrency menjalankan paralel

func main() {
	// concurenccy adalah design pattern (code) yang dimodifikasi sehingga dapat menjalankan 
	// proses parallel pada cpu yang core nya lebih dari 1
	// perbedaan concurrency dan parallelism https://id.quora.com/Apa-perbedaan-konkurensi-dan-paralelisme
	fmt.Println(runtime.GOOS) // nama OS
	fmt.Println(runtime.GOARCH) // nama arsitektur
	fmt.Println(runtime.NumCPU()) // jumlah cpu
	fmt.Println(runtime.NumGoroutine()) // jumlah go routine

	wg.Add(1)
	go foo()
	bar()

	fmt.Println(runtime.NumCPU()) // jumlah cpu
	fmt.Println(runtime.NumGoroutine()) // jumlah go routine
	wg.Wait()

	// coba channel
	ch := make(chan int)
	go func() {
		ch <- doSomething(5)
	}()
	fmt.Println("This is channel", <-ch)

	// Race Condition
	fmt.Println("-----------")
	fmt.Println("CPUs:", runtime.NumCPU())
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	
	counter := 0
	const gs = 100
	// add wait group
	wg.Add(gs)

	for i := 0; i<gs ; i++ {
		go func() {
			v := counter
			// time.Sleep(time.Second)
			runtime.Gosched()
			v++
			counter = v
			wg.Done()
		}()
		fmt.Println("Goroutines:", runtime.NumGoroutine())
	}
	wg.Wait()
	fmt.Println("CPUs:", runtime.NumCPU())
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	fmt.Println("count:", counter)

}

func foo() {
	for i := 0; i<10; i++ {
		fmt.Println("foo:",i)
	}
	wg.Done()
}

func bar() {
	for i := 0; i<10; i++ {
		fmt.Println("bar:",i)
	}
}

func doSomething(x int) int {
	return x*5
}