package main

import (
	"fmt"
)

func main() {
	// exercise_1()
	// exercise_2()
	// exercise_3()
	// exercise_4()
	// exercise_5()
	// exercise_6_7()
	// exercise_8()
	exercise_9()
	exercise_10()
}

func exercise_1() {
	angka := 5 // angka tinggal ganti 10000
	for i:=1; i<=angka;i++ {
		fmt.Printf("%d ", i)
	}
	fmt.Println()
}

func exercise_2() {
	for i:=65;i<=90;i++ {
		fmt.Println(i)
		for j:=0;j<3;j++ {
			fmt.Printf("%#U\n", i)
		}
		fmt.Println()
	}
}

func exercise_3() {
	born := 1996
	now := 2019
	for born<=now {
		fmt.Println(born)
		born++
	}
}

func exercise_4() {
	born := 1996
	now := 2019

	for {
		if born > now {
			break
		}
		fmt.Println(born)
		born++
	}
}

func exercise_5() {
	mod := 4
	for i:=10; i<=100; i++ {
		fmt.Printf("%d ", i%mod)
	}
	fmt.Println()
}

func exercise_6_7() {
	name := "James Bond"

	if name == "Bond" {
		fmt.Println("Thats Bond")
	} else if name == "James Bond" {
		fmt.Println("Thats James Bond")
	} else {
		fmt.Println("Sorry your name has not been in our dictionary")
	}
}


func exercise_8() {
	a := 11
	switch {
		case a == 10:
			fmt.Println(a)
		case true:
			fmt.Println(true)
		default :
			fmt.Println("Thats not number 10")
		
	}
}

func exercise_9() {
	var favSport string = "favSport"
	switch favSport {
		case "James Bond":
			fmt.Println("James Bond")
		case "favSport":
			fmt.Println("My favorite sport is basketball")
		default:
			fmt.Println("I don't know what do you write")
	}
}

func exercise_10() {
	fmt.Println(true && true)
	fmt.Println(true && false)
	fmt.Println(true || true)
	fmt.Println(true || false)
	fmt.Println(!true)
}