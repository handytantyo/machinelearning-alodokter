package main

import (
	"fmt"
)

func main() {
	// exercise_1()
	// exercise_2()
	// exercise_3()
	// exercise_4()
	// exercise_5()
	// exercise_6()
	// exercise_7()
	exercise_8()
}

func exercise_1() {
	// atau var a [5]int
	a := [5]int{}
	a[0] = 1
	a[1] = 20
	a[2] = 13
	a[3] = 4
	a[4] = 1996
	
	fmt.Println(a)

	for index, value := range a {
		fmt.Println(index, value)
	}

	fmt.Printf("%T\n", a)
}

func exercise_2() {
	a:= []int{1,2,3,4,5,6,7,8,9,10}

	for index, value := range a {
		fmt.Println(index, value)
	}

	fmt.Println(a)
}

func exercise_3() {
	x := []int{42,43,44,45,46,47,48,49,50,51}

	fmt.Println(x[:5])
	fmt.Println(x[5:])
	fmt.Println(x[2:7])
	fmt.Println(x[1:6])
	fmt.Println(x)

}

func exercise_4() {
	x := []int{42,43,44,45,46,47,48,49,50,51}
	x = append(x, 52)
	fmt.Println(x)

	x = append(x, 53,54,55)
	fmt.Println(x)

	v := []int{56,57,58,59,60}
	x = append(x, v...)
	fmt.Println(x)
}

func exercise_5() {
	x := []int{42,43,44,45,46,47,48,49,50,51}
	fmt.Println(x)
	
	x = append(x[:3], x[6:]...)
	fmt.Println(x)
}

func exercise_6() {
	name_of_city := make([]string, 40, 50) // array, length, capacity
	name_of_city = []string{` Alabama`, ` Alaska`, ` Arizona`, ` Arkansas`, ` California`, ` Colorado`, ` Connecticut`, ` Delaware`, ` Florida`, ` Georgia`, ` Hawaii`, ` Idaho`, ` Illinois`, ` Indiana`, ` Iowa`, ` Kansas`, ` Kentucky`, ` Louisiana`, ` Maine`, ` Maryland`, ` Massachusetts`, ` Michigan`, ` Minnesota`, ` Mississippi`, ` Missouri`, ` Montana`, ` Nebraska`, ` Nevada`, ` New Hampshire`, ` New Jersey`, ` New Mexico`, ` New York`, ` North Carolina`, ` North Dakota`, ` Ohio`, ` Oklahoma`, ` Oregon`, ` Pennsylvania`, ` Rhode Island`, ` South Carolina`, ` South Dakota`, ` Tennessee`, ` Texas`, ` Utah`, ` Vermont`, ` Virginia`, ` Washington`, ` West Virginia`, ` Wisconsin`, ` Wyoming`}
	fmt.Println(name_of_city)
	fmt.Println(len(name_of_city))
	fmt.Println(cap(name_of_city))

	for index, value := range name_of_city {
		fmt.Println(index, value)
	}
}

func exercise_7() {
	//slice string multidimensional
	a := [][]string{
		{"James","Bond","Shaken, not stired"},
		{"Miss","Moneypenny","Hellooo, James"},
	}

	for index, values := range a {
		fmt.Println(index)
		for indexx, value := range values {
			fmt.Printf("\tindex position: %d, value: %s",indexx, value)
			fmt.Println()
		}
	}
}

func exercise_8() {
	a := map[string][]string{
		"James Harden" : {"James","Bond","Shaken, not stired"},
		"Stephen Curry": {"James","Bond","Shaken, not stired"},
		"Cristiano Ronaldo" : {"Miss","Moneypenny","Hellooo, James"},
	}

	for key, value := range a {
		fmt.Println(key)
		for index, v2 := range value {
			fmt.Printf("\t%d %s\n", index, v2)
		}
	}

	//exercise 9
	a["Feliks Zemdeg"] = []string{"Bakmi","Surya", "Gading Serpong"}
	for index, value := range a {
		fmt.Println(index, value)
	}

	//exercise 10
	delete(a, "James Harden")
	for key, value := range a {
		fmt.Println(key)
		for index, v2 := range value {
			fmt.Printf("\t%d %s\n", index, v2)
		}
	}

}