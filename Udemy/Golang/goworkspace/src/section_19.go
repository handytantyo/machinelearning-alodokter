package main

import (
	"fmt"
	"encoding/json"
	"os"
	"sort"
	// "golang.org/x/crypto/bcrypt"
)

type person struct {
	First string
	Last string
	Age int
}

type user struct {
	First   string
	Last    string
	Age     int
	Sayings []string
}

type ByAge []user
func (ba ByAge) Len() int           { return len(ba) }
func (ba ByAge) Swap(i, j int)      { ba[i], ba[j] = ba[j], ba[i] }
func (ba ByAge) Less(i, j int) bool { return ba[i].Age < ba[j].Age }

type ByLast []user
func (bl ByLast) Len() int           { return len(bl) }
func (bl ByLast) Swap(i, j int)      { bl[i], bl[j] = bl[j], bl[i] }
func (bl ByLast) Less(i, j int) bool { return bl[i].Last < bl[j].Last }

func main() {
	p1 := person {
		First : "aaa",
		Last : "zzz",
		Age : 12,
	}

	p2 := person {
		First : "bbb",
		Last : "yyy",
		Age : 21,
	}

	p3 := person {
		First : "ccc",
		Last : "www",
		Age : 21,
	}

	gabungan := []person{p1, p2, p3}
	fmt.Println(gabungan)
	struct_to_json := exercise1(gabungan)
	fmt.Println(struct_to_json)

	// json to struct
	json_to_struct := []person{}
	s := struct_to_json
	json_to_struct= exercise2(s, json_to_struct)
	fmt.Println(json_to_struct)

	// exercise 3
	u1 := user{
		First: "James",
		Last:  "Bond",
		Age:   32,
		Sayings: []string{
			"Shaken, not stirred",
			"Youth is no guarantee of innovation",
			"In his majesty's royal service",
		},
	}

	u2 := user{
		First: "Miss",
		Last:  "Moneypenny",
		Age:   27,
		Sayings: []string{
			"James, it is soo good to see you",
			"Would you like me to take care of that for you, James?",
			"I would really prefer to be a secret agent myself.",
		},
	}

	u3 := user{
		First: "M",
		Last:  "Hmmmm",
		Age:   54,
		Sayings: []string{
			"Oh, James. You didn't.",
			"Dear God, what has James done now?",
			"Can someone please tell me where James Bond is?",
		},
	}

	users := []user{u1, u2, u3}

	fmt.Println("This is users:",users)

	err := json.NewEncoder(os.Stdout).Encode(users)
	if err != nil {
		fmt.Println("We did something wrong and here's the error:", err)
	}
	fmt.Println(users)


	// exercise 4 sorting
	xi := []int{5, 8, 2, 43, 17, 987, 14, 12, 21, 1, 4, 2, 3, 93, 13}
	xs := []string{"random", "rainbow", "delights", "in", "torpedo", "summers", "under", "gallantry", "fragmented", "moons", "across", "magenta"}

	fmt.Println(xi)
	exercise4_1(xi)
	fmt.Println(xi)

	fmt.Println(xs)
	exercise4_2(xs)
	fmt.Println(xs)

	// sort by ages (key)
	sort.Sort(ByAge(users))
	fmt.Println(users)

	// sort by last (key)
	sort.Sort(ByLast(users))
	fmt.Println(users)
}

func exercise1(p []person) string {
	// convert into json
	a, err := json.Marshal(p)
	if err != nil {
		fmt.Println(err)
	}
	
	return string(a)
}

func exercise2(s string, struct_list []person) []person {
	s_byte := []byte(s)
	err1 := json.Unmarshal(s_byte, &struct_list)
	if err1 != nil {
		fmt.Println(err1)
	}
	for index, value := range struct_list {
		fmt.Println(index, value.First, value.Last, value.Age)
	}

	return struct_list
}

func exercise4_1(i []int) {
	sort.Ints(i)
}

func exercise4_2(s []string) {
	sort.Strings(s)
}