package main

import (
	"fmt"
	"math"
)

type circle struct {
	radius float64
}

type shape interface {
	area() float64
}

func (c *circle) area() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}

func info(s shape) {
	fmt.Println("Area :", s.area())
}

func main() {
	// & memberikan alamat dari variable itu disimpan
	// * memberikan nilai dari alamat
	a := 42
	fmt.Printf("value %d type %T\n", a, a)
	fmt.Printf("value %#x type %T\n", &a, &a) // &a adalah alamat/pointer

	var b *int = &a
	fmt.Println(b)
	fmt.Println(*b)
	fmt.Println(*&a)

	*b = 43
	fmt.Println(a)

	// pointer 2
	aa := 2
	fmt.Printf("value awal %d alamat awal %#x\n", aa, &aa)
	pointer(&aa)
	fmt.Printf("value setelah pointer %d alamat setelah pointer %#x\n", aa, &aa)

	// circle
	// pointer dapat bekerja saat method yang dibuat adalah sebagai berikut : 
	// 1. receiver non pointer, value non pointer
	// 2. receiver non pointer, value pointer
	// 3. receiver pointer, value pointer
	c1 := circle {
		radius : 5,
	}
	info(&c1)
}

func pointer(x *int) {
	*x = 45
}