package main

import (
	"fmt"
	"strconv"
)

// Create own datatype
type halo int
var v halo

var b = 4
// Zero value
// false for booleans, 0 for numeric types,
// "" for strings, and nil for pointers, functions, interfaces, slices, channels, and maps
// kita tidak bisa melakukan perubahan data sepert b = z atau kebalikannya walopun type nya int, 
// karena di program terbaca int dan main.halo
var z halo = 3 


func main(){ // ()-> parens, {}-> curly braces
	n, err := fmt.Println("hello", "world") //all of variable must be used except variable "_", multiple line
	fmt.Println(n)// output int ->byte memory example: 12 -> (12 bytes)
	fmt.Println(err)

	// looping 1
	for i:=0;i<7;i++{
		fmt.Println(i)
	}

	example_function()

	// Declaration variable := , var, var name <datatype>
	// Perbedaan apabila var berlaku untuk variable global sedangkan := untuk variable lokal
	a := 2
	a = 7 // variable a bisa diubah tapi harus int
	fmt.Println(a)
	fmt.Println(b)

	gabung_double_quotes()
	conversion()
}

func example_function(){
	var a string = "function"
	fmt.Printf("This is example of %s %d\n", a, b) //only printf that can use %s %d %f etc
	fmt.Printf("Datatype a = %T\n",a) // printing format datatype
	fmt.Printf("Datatype z = %T\n", z)
}

func gabung_double_quotes(){
	// `` bisa digunakan untuk menggantikan ""
	fmt.Println(`"dsdsd" sdacas`)
}

func conversion(){
	var a int = 3
	var c halo = 4

	fmt.Printf("%d %T\n", a, a)
	fmt.Printf("%d %T\n", c, c)

	a = int(c)
	fmt.Printf("%d %T\n", a, a)

	string_to_int, _ := strconv.Atoi("1234")
	fmt.Printf("%d", string_to_int)
}