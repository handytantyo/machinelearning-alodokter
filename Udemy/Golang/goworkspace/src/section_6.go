package main

import (
	"fmt"
	"runtime"
)

type anyhow int

func main(){
	a := 7
	b := 42
	fmt.Println(a==b)

	// numerical int8 maximum value 127, kalau lebih constant "129" overflows int8
	var x int8 = -128
	fmt.Println(x)
	fmt.Printf("%T\n", x)

	var y anyhow = 5
	fmt.Println(y)

	// liat runtime package
	fmt.Println(runtime.GOOS) // name of operating system
	fmt.Println(runtime.GOARCH) // name of processor

	// string_type()
	// constant()
	// iota_()
	bit_shifting()

}

func string_type(){
	s := "Hello , programming"
	fmt.Println(s)
	fmt.Printf("%T\n",s)

	bs := []byte(s) // making array, kode ascii setiap huruf
	fmt.Println("Array bs = ", bs)
	fmt.Printf("%T\n", bs)
	fmt.Printf("Panjang array bs = %d\n", len(bs)) // length of array

	for i := 0; i<len(s); i++{
		fmt.Printf("%#U\t", s[i]) // printing UTF code point of each character
	}
	fmt.Println()

	// contoh looping for each yang pertama selalu index terlebih dahulu
	for index, value := range s {
		// fmt.Println(index, value)
		fmt.Printf("index %d hex %#x\n", index, value)
	}
}

func constant() {
	// deklarasi variable
	const a = 27
	const b float64 = 5

	const (
		c = 56.5
		d string = "Hello World"
	)

	fmt.Println(a, b, c, d)
}

func iota_() {
	const (
		a = iota
		b 
		c = iota
		d
	)

	const e = iota

	const (
		f = 90
		g = iota
		h = 10
		i
		j = iota
	)

	fmt.Println(a, b, c, d, e, f, g, h, i, j) // constant pertama harus ada deklarasi variable
	fmt.Printf("%T %T %T %T %T %T %T %T %T %T\n", a, b, c, d, e, f, g, h, i, j)
}

func bit_shifting() {
	const (
		_ = iota // inisialisasi awal iota adalah 0
		kb = 1 << (iota * 10) // artinya adalah 2 pangkat 10
		mb = 1 << (iota * 10)
		gb = 1 << (iota * 10)
	)

	fmt.Printf("%d\t\t\t%b\n", kb, kb)
	fmt.Printf("%d\t\t\t%b\n", mb, mb)
	fmt.Printf("%d\t\t%b\n", gb, gb)
}