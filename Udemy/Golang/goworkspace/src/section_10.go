package main

import (
	"fmt"
)

func main() {
	// array()
	// array2()
	// // fmt.Println("2"+"2")
	// array3()
	// array4()
	// array5()
	// array6()
	// array7()
	// array8()
	array9()
}

func array() {
	//var a [5]int -> artinya sama
	a := [5]int{}
	a[3] = 97
	fmt.Println(a)
}

func array2() {
	// composite literal
	// x:= type{values}
	x := []int{4,5,6,7,8,9,10}
	fmt.Println(x)
	for _,v:= range x {
		fmt.Print(v," ")
	}
	fmt.Println()
}

func array3() {
	halo:= "hallo"
	// slicing
	fmt.Printf("%s\n", halo[1:3])
	fmt.Printf("%s\n", halo[1:])
}

func array4() {
	// append to a slice, apabila ingin menggunakan fungsi append, banyak array jgn di deklarasi
	integer := []int{1,2,3,4,5}
	integer = append(integer, 5,4,3,2,10)
	fmt.Println(integer)

	y := []int{3,4,500}
	integer = append(integer, y...) // harus pake ...
	fmt.Println(integer)
}

func array5() {
	// append to a slice, apabila ingin menggunakan fungsi append, banyak array jgn di deklarasi
	integer := []int{1,2,3,4,5}
	fmt.Println("capacity1", cap(integer))
	integer = append(integer, 5,3,10)
	fmt.Println("capacity2", cap(integer))
	fmt.Println(integer)

	// deleting from a slice
	integer = append(integer[:2], integer[3:6]...) // harus pake ...
	fmt.Println(integer)
	fmt.Println(integer[2:])
}

func array6() {
	x := [5]int{1,2,3} // otomatis kalau yang tidak terisi akan diisikan sebagai default
	fmt.Println(x)
}

func array7() {
	x := make([]int, 10, 12) // 12 is maximum array
	// extend its length, extend maximum 12 sesuai deklarasi
	fmt.Println("capacity 1",cap(x))

	x = x[:11]
	x[10] = 1
	fmt.Println("capacity 2",cap(x))

	// alternatif kedua menggunakan append, kelemahan append adalah mengkopi ulang nilai yang sebelumnya ditambah 
	// nilai baru dan membuang nilai yang lama
	x = append(x, 2, 3)

	fmt.Println(x)
	fmt.Println(len(x))
	
	// capacity akan memperbesar sesuai inisialisasi make (lebih dari threshold akan bertambah 12 lagi sesuai yang diinisialisasi)
	fmt.Println("capacity 3",cap(x))
}

func array8() {
	ht := []string{"Handy", "Tantyo", "Salatiga", "Jawa Tengah"}
	fmt.Println(ht)

	bs := []string{"Bakmi", "Surya", "Gading Serpong", "Tangerang"}
	fmt.Println(bs)

	xp := [][]string{ht,bs}
	fmt.Println(xp)

	angka := [][]int{{1,2},{3,4,5},{2,3}}
	fmt.Println(angka)
}

func array9() {
	// maps, keunggulannay tidak ada urutan dan proses search nya sangat cepat
	m := map[string]int{
		"a" : 100,
		"b" : 121,
		"c" : 144,
	}
	fmt.Println(m)
	fmt.Println(m["a"])


	if v, ok := m["a"]; ok { // nilai ok adalah boolean
		fmt.Println(`m["a"] has a value`, v)
	}

	// add element maps, key dan value harus tipe data yang sama
	m["az"] = 169
	
	for i, v := range m {
		fmt.Println(i, v)
	}

	// delete element maps
	delete(m, "b")
	fmt.Println(m)
	
	// multi declaration
	a, b := 2, "Surya"
	fmt.Println(a,b)
}

