package main

import (
	"fmt"
)
var a = 100

func main() {
	// exercise_1()
	// exercise_2()
	// exercise_3()
	// exercise_4()
	exercise_5()
	exercise_6()
}

func exercise_1() {
	fmt.Printf("%d\t%b\t%#x\n", a, a, a)
}

func exercise_2() {
	b := (100 == a)
	c := (100 >= a)
	d := (100 <= a)
	e := (100 != a)
	f := (100 > a)
	g := (100 < a)

	fmt.Println(b, c, d, e, f, g)
}

func exercise_3() {
	const (
		a = 42
		b int = 43
	)
	fmt.Println(a, b)
}

func exercise_4() {
	z := 3
	fmt.Printf("%d\t%b\t%#x\n", z, z, z)

	aa := z << 1
	fmt.Printf("%d\t%b\t%#x\n", aa, aa, aa)
}

func exercise_5() {
	// print string with raw string
	ab :=`here is something
as a raw string literal
"you see" another thing`

	fmt.Println(ab)
}

func exercise_6() {
	const now = 2019
	const (		
		_ = iota
		last_year = now - iota
		last_2_year = now - iota
		last_3_year = now - iota
		last_4_year = now - iota
	)

	fmt.Println(last_year, last_2_year, last_3_year, last_4_year, a)
}

