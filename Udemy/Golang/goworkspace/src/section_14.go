package main

import (
	"fmt"
)

type person struct {
	first string
	last string
}

type secretAgent struct {
	person
	ltk bool
}

func (s secretAgent) speak() {
	fmt.Println("I am", s.first, s.last, "-secret agent")
}

func (p person) speak() {
	fmt.Println("I am", p.first, p.last, "-person")
}

type human interface { // bisa digunakan untuk type(class) lainnya
	speak()
}

func bars(h human) {
	// fmt.Println(" I am human", h)
	// kalo switch type menggunakan .()
	switch h.(type) {
	case person:
		fmt.Println(" I am a person", h.(person).first, h)
	case secretAgent:
		fmt.Println(" I am a secret agent", h.(secretAgent).last,h)
		
	}
}

var closure int

func main() {
	// arguments
	defer syntax() // defer digunakan untuk menunda fungsi tersebut dipanggil, proses akan dipanggil saat program akhir sudah selesai
	bar("Handy") // handy merupakan arguments
	s1 := woo("Haha")
	fmt.Println(s1)

	// multi return
	a, b := mouse("Handy", "Tantyo")
	fmt.Println(a,b)

	// variadic parameter
	sum := variadic_parameter(1,2,3,4,5,6,7,8,9,100)
	fmt.Println(sum)

	// unfurling a slice
	x := []int{1,2,3,4,5,6,7,8,9,100}
	sum1 := variadic_parameter2("Handy", x...) //... didepan artinya memecah semua list/slice yang ada didalam
	fmt.Println(sum1)

	// Methods
	sa1 := secretAgent {
		person : person {
			first : "Handy",
			last : "Tantyo",
		},
		ltk : true,
	}
	sa1.speak()

	//interfaces
	p1 := person {
		first: "Bakmi",
		last : "Surya",
	}

	bars(sa1)
	bars(p1)

	// anonymous function
	func() {
		fmt.Println("Anonymous Function 1")
	}()

	func(x int){
		fmt.Println("My age is", x)
	}(23)

	// func anonymous expression
	f1 := func() {
		fmt.Println("Halo Handyyy")
	}
	f1()

	f2 := func(x int) {
		fmt.Println("My favorite number is", x)
	}
	f2(11)

	f3 := func()int{
		return 100
	}()
	fmt.Println(f3)

	za:= bar3()
	fmt.Printf("%T\n",za) // output should called by
	caa := za()
	fmt.Println(caa)

	sum2 := even(variadic_parameter, x...)
	fmt.Println("Even total is", sum2)

	// applying closure
	fmt.Println(closure)
	closure++
	fmt.Println(closure)
	foo2()
	fmt.Println(closure)

	// closure 2
	aaa := incrementor()
	aab := incrementor()
	fmt.Println(aaa())
	fmt.Println(aaa())
	fmt.Println(aaa())
	fmt.Println(aab())
	fmt.Println(aab())
	fmt.Println(aaa())

	factorial_recursion := factorial(4)
	fmt.Println(factorial_recursion)
	factorial_loop := loopFact(4)
	fmt.Println(factorial_loop)

	
}

func bar3() func() int {
	return func() int {
		return 451
	}
}

func syntax() {
	// func (r receiver) identifier(parameters) (return(s)) {...}
	fmt.Println("This is Function")
}

func bar(s string) {
	fmt.Println("Hello", s) // s merupakan parameter
}

func woo(s string) string{
	return "Hello " + s
	//fmt.Sprint("Hello ", s)
}

func mouse(first string, last string) (string, bool) {
	a := fmt.Sprint("Hi ", first, " ", last)
	b := true

	return a,b
}

func variadic_parameter(x ...int) int{ //... merupakan lexical symbol
	// variadic parameter adalah parameter yang unilimited
	fmt.Print(x, " ")
	fmt.Printf("%T\n",x)

	// sum all the value
	sum := 0
	for _, value := range x {
		sum += value
	}
	return sum
}

func variadic_parameter2(s string, x ...int) int{ //... merupakan lexical symbol
	// variadic parameter adalah parameter yang unilimited
	fmt.Print(x, " ")
	fmt.Printf("%T\n",x)
	fmt.Printf("My name is %s ", s)

	// sum all the value
	sum := 0
	for _, value := range x {
		sum += value
	}
	return sum
}

// callbacks the function (function that pass the value to another function)
func even(f func(xi... int) int, vi... int) int {
	var yi []int
	for _, value := range vi {
		if value % 2 == 0 {
			yi = append(yi, value)
		}
	}

	return f(yi...)
}

// closure
func foo2() {
	fmt.Println("test foo2")
	closure++
}

// closure 2
func incrementor() func() int {
	var x int
	return func() int {
		x++
		return x
	}
}

// recursion
func factorial(x int) int {
	if x == 1 {
		return 1
	}

	return x * factorial(x-1)
}

func loopFact(x int) int {
	total := 1
	for ; x > 0; x-- {
		total *= x
	}
	return total
}