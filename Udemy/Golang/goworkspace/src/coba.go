package main

import (
	"fmt"
	"bufio"
	"os"
)

func main() {
	fmt.Println("Test")

	// cara baca input 1
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter Text: ")
	text, _ := reader.ReadString('\n')
	fmt.Println(text)

	star_tree()

}

func star_tree(){
	// cara baca input 2
	var star int
	fmt.Print("Enter text/integer: ")
	fmt.Scanf("%d", &star)
	fmt.Println(star)
	space := star-1
	for i:=0; i<star; i++ {
		for space_:=0;space_<space;space_++{
			fmt.Printf(" ")
		}

		for j:=0; j<=i; j++ {
			fmt.Printf("* ")
		}
		fmt.Println()
		space--
	}
}