package main

import (
	"fmt"
)

type person struct {
	name string
}

func main() {
	// exercise 1
	a := 7
	fmt.Println(a, &a)

	// exercise 2
	p1 := person {"Alan"}
	changeMe(&p1, "Bill")
	fmt.Println(p1.name)

}

func changeMe(p *person, name string) {
	p.name = name
	// sama artinya dengan (*p).name
}