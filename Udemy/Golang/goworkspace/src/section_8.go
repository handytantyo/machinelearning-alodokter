package main

import (
	"fmt"
)

func main() {
	// Loop init, condition, post
	for i:=0 ; i<=10; i++ {
		fmt.Printf("%d ", i)
	}
	fmt.Println()

	// nested_loop()
	// for_statement()
	// break_continue()
	// ascii()
	// if_statement()
	// if_elseif()
	switch_case()


}

func nested_loop() {
	// Loop nesting loop
	for i:=0; i<=5; i++ {
		for j:=0; j<=i; j++ {
			fmt.Printf("%d ",j)
		}
		fmt.Println()
	}
}

func for_statement() {
	// for statement
	x := 1
	for {
		if x > 10 {
			fmt.Println("done")
			break
		}

		fmt.Printf("%d ",x); fmt.Printf(" h") // untuk membuat 2 line jadi 1 baris
		x++
	}
}

func break_continue() {
	x := 83 / 40
	y := 83 % 40
	fmt.Printf("%d %d\n", x, y)

	i := 0
	for {
		i++
		if i > 10 {
			break
		}

		fmt.Print(i, " ")
		if i % 2 == 0 {
			fmt.Print(true," ")
			continue // lanjut ke for selanjutnya
		} else { // else tidak pakai enter
			fmt.Print(false, " ")
		}

		fmt.Print("test ")

		
	}
	fmt.Println()

}

func ascii() {
	for i:=33; i<=45; i++ {
		fmt.Printf("%v\t%#x\t%#U\n", i, i, i)
	}
}

func if_statement() {
	if x:=42; x!= 43 { // tapi scope x cuma sampai di looping ini saja
		fmt.Println("thats right")
	}
}

func if_elseif() {
	x:= 100
	if x == 99{
		fmt.Println("Awesome")
	} else if x == 100{
		fmt.Println("Perfect")
	} else {
		fmt.Println(x)
	}
}

func switch_case(){
	x := 100
	// case bisa integer, float, boolean maupun string -> untuk boolean yang diambil yang true
	// untuk mengembalikan angka pakai return. misal return x atau return "string"
	switch x {
		case 97:
			fmt.Println("Thats 97")
		case 98:
			fmt.Println("Thats 98")
		case 100:
			fmt.Println("Thats 100")
			fallthrough // memaksa masuk ke case selanjutnya berapapun nilainya, urutan case menentukan untuk fallthrough
		case 99:
			fmt.Println("Thats 99")
		case 2,3,4,5:
			fmt.Println("Thats to small")
		default:
			fmt.Println("I don't know the number")
		
	}
}