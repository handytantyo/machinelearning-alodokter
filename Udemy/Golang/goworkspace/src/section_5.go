package main

import (
	"fmt"
)
// package level scope exercise 3
var a int = 42
var b string = "James Bond"
var c bool = true

type ha int

var x_ ha
var y_ int


func main(){
	x := 42
	y := "James Bond"
	z := true

	fmt.Println(x, y, z)
	exercise_2()

	// exercise 3
	// Fungsi fmt.Sprintf() pada dasarnya sama dengan fmt.Printf(), hanya saja fungsi ini tidak menampilkan nilai, 
	// melainkan mengembalikan nilainya dalam bentuk string. Pada kasus di atas, nilai kembalian 
	// fmt.Sprintf() ditampung oleh variabel msg.
	// Selain fmt.Sprintf(), ada juga fmt.Sprint() dan fmt.Sprintln().
	s := fmt.Sprintf("%v %v %v", a,b,c)
	fmt.Println(s)

	// int to string
	int_to_string := fmt.Sprintf("%v",x)
	fmt.Printf("%s %T\n",int_to_string, int_to_string)

	exercise_4()
	exercise_5()

}

func exercise_2(){
	var x int
	var y string
	var z bool

	fmt.Printf("%T %T %T\n", x, y, z)
	fmt.Println(x, y, z)
}

func exercise_4(){
	var x ha
	fmt.Println(x)
	fmt.Printf("%T\n",x)

	x = 42
	fmt.Println(x)
}

func exercise_5(){
	fmt.Println(x_)
	fmt.Println(y_)

	x_ = 10
	fmt.Println(x_)
	y_ = int(x_)
	fmt.Println(y_)
	fmt.Printf("%T\n",y_)
}