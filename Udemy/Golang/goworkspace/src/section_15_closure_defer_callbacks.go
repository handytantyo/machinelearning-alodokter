package main

import (
	"fmt"
	"math"
)

type person struct {
	first string
	last string
	age int
}

func (p person) speak() {
	fmt.Printf("Hi my name is %s %s. I am %d years old\n", p.first, p.last, p.age)
}

type square struct {
	side float64
}

type circle struct {
	radius float64
}

func (s square) area() float64 {
	return s.side * s.side
}

func (c circle) area() float64 {
	return c.radius * c.radius * math.Pi
}

type shape interface {
	area() float64 // karena mengembalikan nilai, kalau tidak ya ngga perlu kasi type variable nya
}

func info(s shape) {
	switch s.(type) {
	case circle:
		fmt.Println("Area Circle :", s.area())
	case square:
		fmt.Println("Area Square :", s.area())
		
	}
}

var closure int

func main() {
	defer print_with_defer()

	foo_function := foo(5)
	fmt.Println(foo_function)

	bar_int, bar_string := bar(11, "Thompson")
	fmt.Println(bar_int, bar_string)

	x := []int{23,54,768,124,1231,33}
	sum1 := foo_variadic_parameter(x...)
	fmt.Println(sum1)

	x2 := []int{23,54,768,124,1231,33}
	sum2 := bar_variadic_parameter(x2)
	fmt.Println(sum2)

	// exercise 4
	p1 := person {
		first : "Handy",
		last : "Tantyo",
		age : 23,
	}

	p1.speak()

	//exercise 5
	fmt.Println("------")
	square1 := square {
		side : 5,
	}
	fmt.Println(square1.area())

	circle1 := circle {
		radius : 32.4,
	}
	fmt.Println(circle1.area())

	info(square1)
	info(circle1)

	// exercise 6
	x6 := func() {
		fmt.Println("This is exercise 6")
	}
	x6()

	func() {
		fmt.Println("This is exercise 6_2")
	}()

	// exercise 7
	x7 := func(x int) int {
		return x + 3
	}
	fmt.Println(x7(7))
	fmt.Printf("type x7 is %T\n", x7)

	//exercise 8
	x8 := exercise_8()
	fmt.Println(x8())

	// exercise 9, callbacks
	g := func(xi []int) int {
		if len(xi)== 0 {
			return 0
		} else if len(xi) == 1{
			return xi[0]
		}
		return xi[0] + xi[len(xi)-1]
	}
	
	x9 := exercise_9(g, []int{1,2,3,4,5})
	fmt.Println(x9)

	// exercise 10, closure
	fmt.Println(closure)
	closure_()
	fmt.Println(closure)
	closure_()

	//exercise 11
	decimal := 10
	fmt.Println(dec_to_binary(decimal))

}

func foo(x int) int {
	return x
}

func bar(x int, y string) (int, string) {
	return x, y
}

func foo_variadic_parameter(x ...int) int {
	sum := 0
	for _, value := range x {
		sum += value
	}

	return sum
}

func bar_variadic_parameter(x []int) int {
	sum := 0
	for _, value := range x {
		sum += value
	}

	return sum
}

func print_with_defer() {
	defer func() {
		fmt.Println("Defer in defer")
	}()
	fmt.Println("This is defer function")
}

func exercise_8() func() int {
	return func() int {
		return 100
	}
}

// callbacks
func exercise_9(f func(xi []int) int, ii []int) int {
	n := f(ii)
	n++
	return n
} 

// closure
func closure_() {
	fmt.Println("this is closure")
	closure++
}

// recursive binary
func dec_to_binary(x int) int {
	if x <= 1 {
		return 1
	}

	return 10 * dec_to_binary(x/2) + (x % 2)
}