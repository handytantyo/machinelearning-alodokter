package main

import (
	"fmt"
)

type mahasiswa struct {
	Nama_lengkap string
	Nama_panggilan string
	Umur int
	Alamat string
	Jurusan string
	Keterangan map[string]string

}

func (m *mahasiswa) NamaLengkap() {
	fmt.Println("Nama saya adalah", m.Nama_lengkap)
}

func (m *mahasiswa) setUmur(x int) {
	m.Umur = x
}

func main() {
	anak1 := mahasiswa {
		Nama_lengkap : "Handy Tantyo",
		Nama_panggilan : "Handy",
		Umur : 18,		
		Jurusan : "Human Computer Interaction",
		Keterangan : map[string]string {
			"makanan" : "nasi goreng",
			"minuman" : "air putih",
			"game" : "rubik",
		},
	}

	fmt.Println(anak1)
	anak1.NamaLengkap()
	anak1.setUmur(23)
	fmt.Println("Umur:", anak1.Umur)

	var input int
	fmt.Printf("Input nilai: ")
	fmt.Scanf("%d", &input)

	maze1(input)

}

func maze1(input int) {
	// making dynamic array
	array := make([][]string, input)
	for index, _ := range array {
		array[index] = make([]string, input)
	}

	// find max iter
	max_iter := input / 2
	if input % 2 != 0{
		max_iter += 1
	}

	// fill the maze
	var min_row, min_column int
	max_row, max_column := input, input

	for iter := 0; iter < max_iter; iter++ {
		for row := min_row ; row < max_row; row++ {
			for column := min_column; column < max_column; column++ {
				if iter % 2 == 0 {
					array[row][column] = "*"
				}else {
					array[row][column] = " "
				}
			}
		}
		min_row++ ; min_column++
		max_row-- ; max_column--
	}

	// Print the result
	for _, i := range array {
		for _, j := range i {
			fmt.Print(j)
		}
		fmt.Println()
	}
}

