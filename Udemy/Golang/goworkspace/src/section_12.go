package main

import (
	"fmt"
)

type person struct {
	first string
	last string
	age int
}

type secretAgent struct {
	person
	first string
	ltk bool
}

func main() {
	// Struct()
	Struct2()

	x, y := 42,43
	x, y = y, x // swap value
	fmt.Println(x, y)

	Struct3()
}

func Struct() {
	p1 := person{
		first : "Handy",
		last : "Tantyo",
		age : 23,
	}
	
	p2 := person{
		first : "James",
		last : "Bond", //bila tidak diisi dengan lengkap maka yang keluar adalah nilai default
	}

	fmt.Println(p1)
	fmt.Println(p2)
	fmt.Println(p1.first, p1.last)
	fmt.Println(p2.first, p2.last)
}

func Struct2() {
	p1 := secretAgent{
		person : person {
			first : "Handy",
			last : "Tantyo",
			age : 23,
		},
		ltk : true, 
	}

	p2 := secretAgent{
		person : person {
			first : "James",
			last : "Bond", //bila tidak diisi dengan lengkap maka yang keluar adalah nilai defaults
							//variabel ini akan di promoted ke higher level sama dengan struct lainnya
							// kecuali di overwrite dengan variabel yang sama -> dipanggil menjadi p2.person.nama_variable
		},
		first : "Lucky",
	}

	fmt.Println(p1)
	fmt.Println(p2)
	fmt.Println(p1.first, p1.last, p1.age, p1.ltk)
	fmt.Println(p2.person.first,"aka",p2.first, p2.last, p2.age, p2.ltk)
}

func Struct3() {
	//Anonymous structs, disebut anonymous karena tidak punya nama(identifier)
	p1 := struct {
		first string
		last string
		age int
	}{
		first : "James",
		last : "Bond",
		age : 32,
	}

	fmt.Println(p1)
}