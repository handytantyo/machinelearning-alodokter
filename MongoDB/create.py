import pymongo
from pandas import DataFrame

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["karyawan"]
mycol = mydb["nama"]

print(mydb)
print(mydb.list_collection_names())
#name = myclient.list_collection_names()
#print(name)

#contoh insert(insert_one and insert_many)
    #contoh untuk insert_one
mydict = { "_id":2,"name": "Alan", "address": "Highway 371" }
    #contoh untuk insert_many 1
insert_many1 = mycol.insert_many(
    [
        {
            '_id': 4, 'name':'asda', 'address':'asdasdas'
        },
        {
            '_id': 5, 'name':'asda', 'address':'asdasdas'
        }
    ]
)
    
    #contoh untuk insert_many 2
id_ = [i for i in range(1,4)]
nama = ['asdas','casac','dasdas']
address = ['jdjksdf','jdkna','jhofd']
dict_ = DataFrame({'id':id_,'nama':nama,'address':address})

    #eksekusi insert_one
#x = mycol.insert_one(mydict)
    #eksekusi insert_many
#x = mycol.insert_many(dict_.to_dict('records'))#insert_many

#print(x)