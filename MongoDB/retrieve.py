import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["karyawan"]
mycol = mydb["nama"]

#retrieve/find/read
#panggil salah satu data saja, data yang paling atas
x = mycol.find_one()
print(x)

#panggil dengan criteria
#y = mycol.find({"_id":ObjectId('5bc6fa19d9f29f58dae5f1d8')})

#tampilin semua

#0 berarti tidak ditampilkan 1 ditampilkan
for x in mycol.find({},{ "_id": 0, "name": 1, "address": 1 }):
  print(x)
